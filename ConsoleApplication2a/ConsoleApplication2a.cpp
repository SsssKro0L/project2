// Lab2.cpp: определяет точку входа для консольного приложения.

//Массивы и указатели

#include "stdafx.h"

#include <iostream>

using namespace std;

// переменная, хранящая адрес другой переменной в памяти, называется указателем (pointer)

int main()

{

	int a = 231;

	double dbl = 10.005;

	// синтаксис объявления указателей-(указатель - ссылаемся на область памяти))

	int * ptr_int = &a;

	double * ptr_dbl = &dbl;

	// все указатели всегда одной и той же разрядности(размера)

	cout << "int pointer size = " << sizeof(ptr_int) << endl;

	cout << "char pointer size= " << sizeof(char*) << endl;

	cout << "long long pointer size= " << sizeof(long long *) << endl;

	cout << "bool pointer size= " << sizeof(bool *) << endl;

	cout << "double pointer size= " << sizeof(ptr_dbl) << endl;

	// содержимое указателей

	//при запуске указатели у всех разные,потому что

	// в чужую область памяти влезть нельзя

	cout << endl << "ptr_int = 0x" << hex << ptr_int << endl;

	// std::hex включает отображение целых чисел по 16-ричной системе

	// пока не будет включена другая система счисления подобным образом все числа

	// будут печататься в х16 системе

	cout << "ptr_ch = 0x" << ptr_dbl << endl;

	//в дальнейшем будет показано, что каким бы ни был огромным тип данных(строка, объект, массив)

	//указатель на него всегда будет занимать 4 / 8 байт, что и является основным преимуществом указателей

	// оператор разыменования указателей: чтобы получить значение , хранящиеся по данному указателю,

	//нужно еще раз применить оператор *

	cout << endl << "ptr_int = " << dec << *ptr_int << endl;

	cout << "ptr_dbl = " << *ptr_dbl << endl;

	// на массивах строится 3D графика

	// массив - блок из нескольких однотипных данных

	// задавать массивом удобно:

	//1) вектора и матрицы

	//2) мн-во точек из 3D и 2D геометрии

	//3) просто какой-либо числовой ряд, таблица и тд.

	//4) строка - массив символов

	//5) видеобуфер(двумерный массив, соответствующий пикселям экрана)

	// int arr[5]; //

	//int arr[5] = {0}; // инициализируется массив с помощью фигурных скобок

	int arr[5] = { 1 , 50 , 11, 12, 1000 }; // одномерный массив из 10 новых знаковых чисел

											// в ОП массив хранится компактно ( без разрывов,элемент за элементом)

											// нумеруются массивы с 0 до (длина - 1)

	cout << arr[0] << '\t' <<

		arr[1] << '\t' << // в одинарных ковычках - только один символ (char), в двойных- любое мн-во

		arr[2] << '\t' << // в общем случае можно везде использовать двойные

		arr[3] << '\t' <<

		arr[4] << endl;

	cout << " arr = " << arr << endl;

	cout << "*arr = " << *arr << endl;

	cout << "*(arr+4) = " << *(arr + 4) << endl;

	for (int *i = &a - 16;

		i < &a + 16;

		i++)

	{

		cout << "0x" << i << '\t' << *i << endl;

	}

	//идентификатор (имя) массива, взятая без индекса, само является указателем

	int arr2D[3][3] = { { 1, 2, 3 },

	{ 4, 5, 6 },

	{ 7, 8, 9 } };

	// если бы хотели занулить весь массив, написали бы int arr2D[3][3] = {0}

	// размерности массивов задаются статически, нельзя задать размер массива с помощью переменной

	// для этого нужно воспользоваться динамической памятью и совсем другим синтексисом объявления

	cout << endl;

	for (int i = 0; i < 3; i++)

	{

		for (int j = 0; j < 3; cout << arr2D[i][j] << '\t', j++); { }

		cout << endl;

	}

	cout << "*(*(arr2D+2)+2) = arr2D[2][2] = " << *(*(arr2D + 2) + 2) << endl;

	// статические массивы в ОП представляю собой компактное сплошное мн-во ( без разрывов)

	/* cout << arr2D[0][0] << '\t' << arr2D[0][1] << '\t' << arr2D[0][2] << '\t' << endl <<

	arr2D[1][0] << '\t' << arr2D[1][1] <<'\t' << arr2D[1][2] << '\t' << endl <<

	arr2D[2][0] << '\t' << arr2D[2][1]<< '\t' << arr2D[2][2] << '\t' << endl;*/

	// arr2D[1] двумерный массив взятый с одной квадратной скобкой это тип *int -указатель на одну строчку одномерный массив внутри двумерного

	// arr2D[1][1] INT двойные скобки - два указателя разыменует его дважды

	//**int указатель на указатель

	getchar();

	return 0;

}

